﻿using Entities.SOLID.Entities;
using Entities.SOLID.Interfaces.IMoeda;

namespace Entities.SOLID.Services
{
    public class NomeMoedaService : INomeMoedaService
    {
        public bool IsValid(string moeda, Bd bd)
        {
            var busca = bd.Moedas.Find(c => c.Nome == moeda);
            if (busca == null)
                return false;
            return true;
        }
    }
}