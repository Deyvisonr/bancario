﻿using Entities.SOLID.Interfaces.IConta;

namespace Entities.SOLID.Services
{
    public class NumeroContaService : INumeroConta
    {
        public bool IsValid(string numero)
        {
            return numero.Length == 4;
        }
    }
}