﻿using Entities.SOLID.Entities;
using Entities.SOLID.Interfaces.IMoeda;

namespace Entities.SOLID.Services
{
    public class MoedaService : IMoedaService
    {
        private readonly IMoedaRepository _moedaRepository;

        public MoedaService(IMoedaRepository moedaRepository)
        {
            _moedaRepository = moedaRepository;
        }
        public string CriarMoeda(Moeda moeda, Bd bd)
        {
            if (!moeda.IsValid(bd))
                return "Dados Inválidos";
            _moedaRepository.CriarMoeda(moeda, bd);
            return "Dados Cadastrados com Sucesso!";
        }
    }
}