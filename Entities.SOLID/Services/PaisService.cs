﻿using System.Collections.Generic;
using System.Linq;
using Entities.SOLID.Interfaces.IMoeda;

namespace Entities.SOLID.Services
{
    public class PaisService : IPaisService
    {
        private List<string> List;

        public PaisService(List<string> list)
        { 
            List = list;
        }

        public bool IsValid(string pais)
        {
            List.Add("Brasil");
            List.Add("Argentina");
            List.Add("Estados Unidos");

            if (List.All(c => c != pais))
                return false;
            return true;
        }
    }
}