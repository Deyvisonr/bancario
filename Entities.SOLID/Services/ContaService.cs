﻿using Entities.SOLID.Entities;
using Entities.SOLID.Interfaces.IConta;

namespace Entities.SOLID.Services
{
    public class ContaService : IContaService
    {
        private readonly IContaRepository _contaRepository;

        public ContaService(IContaRepository contaRepository)
        {
            _contaRepository = contaRepository;
        }
        public string CriarConta(Conta conta, Bd bd)
        {
            if (!conta.IsValid())
                return "Dados Inválidos";
            _contaRepository.CriarConta(conta, bd);

            return "Dados cadastrados com sucesso";
        }
    }
}