﻿using Entities.SOLID.Interfaces.IMoeda;

namespace Entities.SOLID.Entities
{
    public class Moeda
    {
        private readonly INomeMoedaService _nomeMoedaService;
        private readonly IPaisService _paisService;

        public Moeda(INomeMoedaService nomeMoedaService,
            IPaisService paisService)
        {
            _nomeMoedaService = nomeMoedaService;
            _paisService = paisService;
        }
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Pais { get; set; }

        public bool IsValid(Bd bd)
        {
            return _nomeMoedaService.IsValid(Nome, bd) && _paisService.IsValid(Pais);
        }
    }
}
