﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.SOLID.Interfaces.IConta;

namespace Entities.SOLID.Entities
{
    public class Conta
    {
        private readonly INumeroConta _numeroConta;

        public Conta(INumeroConta numeroConta)
        {
            _numeroConta = numeroConta;
        }
        public string NumeroConta { get; set; }
        public string Titular { get; set; }
        public List<Operacao> Operacoes { get; set; }
        public bool IsValid()
        {
            return _numeroConta.IsValid(NumeroConta);
        }
    }
}
