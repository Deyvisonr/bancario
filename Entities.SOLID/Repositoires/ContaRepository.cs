﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.SOLID.Entities;
using Entities.SOLID.Interfaces.IConta;

namespace Entities.SOLID.Repositoires
{
    public class ContaRepository : IContaRepository
    {
        public void CriarConta(Conta conta, Bd bd)
        {
            bd.Contas.Add(conta);
        }
    }
}
