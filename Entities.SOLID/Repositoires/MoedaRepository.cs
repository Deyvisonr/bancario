﻿using Entities.SOLID.Entities;
using Entities.SOLID.Interfaces.IMoeda;

namespace Entities.SOLID.Repositoires
{
    public class MoedaRepository : IMoedaRepository
    {
        public void CriarMoeda(Moeda moeda, Bd bd)
        {
            bd.Moedas.Add(moeda);
        }
    }
}