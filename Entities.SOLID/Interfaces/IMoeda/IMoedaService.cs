﻿using Entities.SOLID.Entities;

namespace Entities.SOLID.Interfaces.IMoeda
{
    public interface IMoedaService
    {
        string CriarMoeda(Moeda moeda, Bd bd);
    }
}