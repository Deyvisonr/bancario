﻿using Entities.SOLID.Entities;

namespace Entities.SOLID.Interfaces.IMoeda
{
    public interface INomeMoedaService
    {
        bool IsValid(string moeda, Bd bd);
    }
}