﻿namespace Entities.SOLID.Interfaces.IMoeda
{
    public interface IPaisService
    {
        bool IsValid(string pais);
    }
}