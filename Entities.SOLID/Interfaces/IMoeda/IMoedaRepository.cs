﻿using Entities.SOLID.Entities;

namespace Entities.SOLID.Interfaces.IMoeda
{
    public interface IMoedaRepository
    {
        void CriarMoeda(Moeda moeda, Bd bd);
    }
}