﻿using Entities.SOLID.Entities;

namespace Entities.SOLID.Interfaces.IConta
{
    public interface IContaRepository
    {
        void CriarConta(Conta conta, Bd bd);
    }
}