﻿using Entities.SOLID.Entities;

namespace Entities.SOLID.Interfaces.IConta
{
    public interface INumeroConta
    {
        bool IsValid(string numero);
    }
}