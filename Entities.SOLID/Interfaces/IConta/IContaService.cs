﻿using Entities.SOLID.Entities;

namespace Entities.SOLID.Interfaces.IConta
{
    public interface IContaService
    {
        string CriarConta(Conta conta, Bd bd);
    }
}