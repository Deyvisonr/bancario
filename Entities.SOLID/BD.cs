﻿using System.Collections.Generic;
using Entities.SOLID.Entities;

namespace Entities.SOLID
{
    public class Bd
    {
        public List<Conta> Contas { get; set; }
        public List<Moeda> Moedas { get; set; } 
         
    }
}