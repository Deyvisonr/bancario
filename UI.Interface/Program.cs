﻿using System;
using System.Collections.Generic;
using Entitites.Domain;

namespace UI.Interface
{
    class Program
    {
        static void Main(string[] args)
        {

            var currencies = new Currency();

            currencies.GerarMoeda("BRL");
            currencies.GerarMoeda("USD");
            currencies.GerarMoeda("ZWL");
            currencies.GerarMoeda("MZN");

            var account = new Account();

            var acc = new Account();

            account.Deposit(currencies.FindCurrency("BRL"), 50M);
            account.Deposit(currencies.FindCurrency("BRL"), 50M);
            account.Deposit(currencies.FindCurrency("BRL"), 100M);
            account.Withdraw(currencies.FindCurrency("BRL"), 100M);

            account.Deposit(currencies.FindCurrency("USD"), 200M);
            account.Deposit(currencies.FindCurrency("USD"), 50M);
            account.Deposit(currencies.FindCurrency("USD"), 50M);

            var balance = account.GetBalance(currencies.FindCurrency("BRL"));
            var balance2 = account.GetBalance(currencies.FindCurrency("USD"));

            acc.Deposit(currencies.FindCurrency("BRL"), 30M);
            acc.Deposit(currencies.FindCurrency("BRL"), 30M);
            acc.Deposit(currencies.FindCurrency("BRL"), 80M);
            acc.Withdraw(currencies.FindCurrency("BRL"), 100M);

            acc.Deposit(currencies.FindCurrency("USD"), 26M);
            acc.Deposit(currencies.FindCurrency("USD"), 25M);
            acc.Deposit(currencies.FindCurrency("USD"), 15M);

            var balance3 = acc.GetBalance(currencies.FindCurrency("BRL"));
            var balance4 = acc.GetBalance(currencies.FindCurrency("USD"));

            var conversoes = new Converter();
            var dinheiroTeste = new Money();

            dinheiroTeste = conversoes.ObterConversao("BRL", 10M , "MZN", currencies);

            var extrato = account.Operations;

            Console.WriteLine(balance.Value + balance.Currency.Name);
            

            Console.ReadKey();
        }
    }
    
}
