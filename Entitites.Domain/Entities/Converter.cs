﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Jarloo.CardStock.Helpers;
using Jarloo.CardStock.Models;

namespace Entitites.Domain
{
    public class Converter
    {
        private readonly DispatcherTimer timer = new DispatcherTimer(DispatcherPriority.Background);

        public ObservableCollection<Quote> Quotes { get; set; }

        public Money ObterConversao(string entrada, decimal value, string saida, Currency currencies)
        {
            decimal contacao = ObterCotacao(entrada, saida);
            var money = new Money().GerarMoney(currencies.FindCurrency(saida), value*contacao);
            return money;
        }

        private decimal ObterCotacao(string entrada, string saida)
        {
            string symbol = entrada.ToUpper() + saida.ToUpper() + "=X";

            Quotes = new ObservableCollection<Quote>();

            //Some example tickers
            Quotes.Add(new Quote(symbol));

            //get the data
            YahooStockEngine.Fetch(Quotes);

            //poll every 25 seconds
            timer.Interval = new TimeSpan(0, 0, 25);
            timer.Tick += (o, e) => YahooStockEngine.Fetch(Quotes);

            timer.Start();

            decimal cotacao = Quotes.FirstOrDefault(c => c.Symbol == symbol).LastTradePrice;
            return cotacao;
        }




    }
}
