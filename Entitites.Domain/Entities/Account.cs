﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Entitites.Domain
{
    public class Account
    {
        public string Name { get; set; }

        public int Number { get; set; }

        public List<Operation> Operations;

        public Account()
        {
            Operations = new List<Operation>();
        }
        public void Deposit(Currency currency, decimal value)
        {
            var money = new Money().GerarMoney(currency, value);
            var op = new Operation().GerarLancamento(money, "Deposit");
            Operations.Add(op);
        }
        public void Withdraw(Currency currency, decimal value)
        {
            var money = new Money().GerarMoney(currency, value);
            var op = new Operation().GerarLancamento(money, "WithDraw");
            Operations.Add(op);
        }

        public Money GetBalance(Currency currency)
        {
            decimal value = Operations.Where(c => c.Money.Currency == currency && c.Type == "Deposit" ).
                Aggregate<Operation, decimal>(0, (current, op) => current + op.Money.Value);

            decimal value1 = Operations.Where(c => c.Money.Currency == currency && c.Type == "WithDraw").
                Aggregate<Operation, decimal>(0, (current, op) => current - op.Money.Value);

            Money money = new Money()
            {
                Currency = currency,
                Value = value + value1
            };

            return money;
        }
    }
}