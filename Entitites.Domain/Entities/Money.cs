﻿namespace Entitites.Domain
{
    public class Money
    {
        public Currency Currency { get; set; }
        public decimal Value { get; set; }

        public Money GerarMoney(Currency currency, decimal value)
        {
            Money money = new Money()
            {
                Currency = currency,
                Value = value
            };

            return money;
        }

    }
}
