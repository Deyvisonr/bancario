﻿using System;

namespace Entitites.Domain
{
    public class Operation
    {
        public Money Money { get; set; }
        public DateTime Time { get; set; }
        public string Type { get; set; }

        public Operation GerarLancamento(Money money, string tipo)
        {
            var op = new Operation()
            {
                Money = money,
                Time = DateTime.Now,
                Type = tipo
            };

            return op;
        }
    }
}
