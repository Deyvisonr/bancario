﻿using System.Collections.Generic;
using System.Linq;


namespace Entitites.Domain
{
    public class Currency
    {
        

        public List<Currency> Currencies;

        public Currency()
        {
            Currencies = new List<Currency>();
        }

        public string Name { get; set; }

        public void GerarMoeda(string name)
        {
            var currency = new Currency()
            {
                Name = name
            };

            Currencies.Add(currency);
        }


        public Currency FindCurrency(string name)
        {
            var currency = Currencies.First(c => c.Name == name);
            return currency;
        }

    }
}
